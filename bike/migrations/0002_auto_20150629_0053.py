# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bike', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bikeroute',
            name='author',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='author'),
        ),
        migrations.AddField(
            model_name='bikeroute',
            name='users_joined',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='users_joined'),
        ),
    ]
