# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bike', '0003_auto_20150701_0347'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.CharField(max_length=127)),
                ('created_at', models.DateTimeField()),
            ],
        ),
        migrations.AlterField(
            model_name='bikeroute',
            name='location_begin_lat',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='bikeroute',
            name='location_begin_lng',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='bikeroute',
            name='location_end_lat',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='bikeroute',
            name='location_end_lng',
            field=models.FloatField(),
        ),
        migrations.AddField(
            model_name='notification',
            name='bike_route',
            field=models.ForeignKey(to='bike.BikeRoute'),
        ),
        migrations.AddField(
            model_name='notification',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
    ]
