# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BikeRoute',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=127)),
                ('description', models.CharField(max_length=1023)),
                ('date_pub', models.DateTimeField(verbose_name='publication_date')),
                ('date_start', models.DateTimeField(verbose_name='expired date')),
                ('location_begin', models.CharField(max_length=127)),
                ('location_end', models.CharField(max_length=127)),
            ],
        ),
    ]
