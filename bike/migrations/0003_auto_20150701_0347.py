# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bike', '0002_auto_20150629_0053'),
    ]

    operations = [
        migrations.AddField(
            model_name='bikeroute',
            name='location_begin_lat',
            field=models.FloatField(default=54.74054),
        ),
        migrations.AddField(
            model_name='bikeroute',
            name='location_begin_lng',
            field=models.FloatField(default=55.985577),
        ),
        migrations.AddField(
            model_name='bikeroute',
            name='location_end_lat',
            field=models.FloatField(default=54.74054),
        ),
        migrations.AddField(
            model_name='bikeroute',
            name='location_end_lng',
            field=models.FloatField(default=55.985577),
        ),
        migrations.AddField(
            model_name='bikeroute',
            name='users_joined_count',
            field=models.IntegerField(default=0),
        ),
    ]
