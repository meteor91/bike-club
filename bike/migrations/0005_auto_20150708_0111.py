# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bike', '0004_auto_20150707_2110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bikeroute',
            name='author',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='bike_routes'),
        ),
        migrations.AlterField(
            model_name='bikeroute',
            name='users_joined',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='bike_routes_joined'),
        ),
        migrations.RemoveField(
            model_name='notification',
            name='user',
        ),
        migrations.AddField(
            model_name='notification',
            name='user',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='notifications'),
        ),
    ]
