# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bike', '0006_auto_20150708_0135'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='notified',
            field=models.BooleanField(default=False),
        ),
    ]
