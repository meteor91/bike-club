# -*- coding: utf-8 -*-
# Create your views here.
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import redirect
from django.views import generic
from django.views.generic import DetailView, FormView, ListView
from django.views.generic.edit import UpdateView
from django.http import JsonResponse
from django.forms import Form
import datetime

from bike.forms import BikeRoutesCreateForm
from bike.models import BikeRoute, Notification


class BikeRouteListView(generic.ListView):
    template_name = 'bike/index.html'
    context_object_name = 'latest_bikeroutes_list'
    paginate_by = 5

    def get_queryset(self):
        return BikeRoute.objects.order_by('-date_pub')


class BikeRouteCreateView(generic.CreateView):
    template_name = 'bike/create.html'
    model = BikeRoute
    form_class = BikeRoutesCreateForm
    success_url = '/'

    def form_valid(self, form):
        form.instance.date_pub = datetime.datetime.now()
        form.instance.author = self.request.user
        return super(BikeRouteCreateView, self).form_valid(form)


class BikeRouteUpdateView(UpdateView):
    template_name = 'bike/update.html'
    model = BikeRoute
    form_class = BikeRoutesCreateForm

    def dispatch(self, request, *args, **kwargs):
        bike_route = BikeRoute.objects.filter(id=kwargs['pk']).first()
        if bike_route is None or self.request.user.id != bike_route.author_id:
            return redirect(reverse('index'))
        return super(BikeRouteUpdateView, self).dispatch(request, args, kwargs)

    def get_success_url(self):
        return reverse_lazy('detail', args=[self.kwargs['pk']])

    def form_valid(self, form):
        result = super(BikeRouteUpdateView, self).form_valid(form)
        if len(form.changed_data) != 0:
            message = generate_notification_message(form)
            users = self.object.users_joined.all()
            for user in users:
                notification = Notification(message=message, created_at=datetime.datetime.now(),
                                            user=user, bike_route=self.object)
                notification.save()
        return result


class BikeRouteDetailView(DetailView):
    model = BikeRoute
    template_name = 'bike/detail.html'

    def get_context_data(self, **kwargs):
        context = super(BikeRouteDetailView, self).get_context_data(**kwargs)
        context['joined'] = False if self.object.users_joined.filter(id=self.request.user.id).first() is None else True

        if self.request.user.id == self.object.author_id:
            context['update'] = reverse('update', kwargs={'pk': self.object.id})
        return context


class NotificationListView(ListView):
    template_name = 'bike/notification_list.html'
    context_object_name = 'actual_notification_list'

    def get_queryset(self):
        return Notification.objects.filter(user_id=self.request.user.id)
        #Notification.objects.filter(user_id=self.request.user.id).update(notified=True)


class JoinView(FormView):
    form_class = Form

    def form_valid(self, form):
        data = {}
        # need to handle exception
        bike_route = BikeRoute.objects.get(id=self.kwargs['br_id'])
        if bike_route.users_joined.filter(id=self.request.user.id).first() is None:
            bike_route.users_joined.add(self.request.user)
            data['result'] = 'added'
        else:
            bike_route.users_joined.remove(self.request.user)
            data['result'] = 'removed'

        new_count = bike_route.users_joined.count()
        bike_route.users_joined_count = new_count
        bike_route.save()
        data['new_count'] = new_count
        return JsonResponse(data)

    def form_invalid(self, form):
        data = {
            'code': 'error',
        }
        print(self.kwargs['br_id'])
        return JsonResponse(data)


def generate_notification_message(form):
    message = []
    for field in form.changed_data:
        field_label = form[field].label
        message.append(field_label)

    return ', '.join(message) + ' было изменено' if len(message) == 1 else 'были изменены.'