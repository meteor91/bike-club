# -*- coding: utf-8 -*-
from django.contrib import admin
from bike.models import BikeRoute
from bike.forms import BikeRoutesCreateForm
import datetime
# Register your models here.


class BikeRouteAdmin(admin.ModelAdmin):
    form = BikeRoutesCreateForm

    fieldsets = (
        (None, {
            'fields': ('title', 'description', 'date_pub', 'date_start',
                       'location_begin', 'location_end', 'google_map_field'),
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        obj.date_pub = datetime.datetime.now()
        super(BikeRouteAdmin, self).save_model(request, obj, form, change)

admin.site.register(BikeRoute, BikeRouteAdmin)
