/**
 * Created by bilal on 26.06.15.
 */

var pinColors = {
    'default': "19BF2F",
    'begin': "FE7569",
    'end': "19BF2F"
}
var pinColor = "19BF2F";
var pinColor2 = "FE7569";
var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
    new google.maps.Size(40, 37),
    new google.maps.Point(0, 0),
    new google.maps.Point(12, 35));
var image_green = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
    new google.maps.Size(21, 34),
    new google.maps.Point(0, 0),
    new google.maps.Point(10, 34));
var image_red = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor2,
    new google.maps.Size(21, 34),
    new google.maps.Point(0, 0),
    new google.maps.Point(10, 34));

var marker_images = {};
marker_images['begin'] = image_red;
marker_images['end'] = image_green;

var map = {};
var geocoder = new google.maps.Geocoder();

var markers = {};
var ufa_coord = new google.maps.LatLng(54.740540, 55.985577);



function initialize() {

    var mapOptions = {
        zoom: 13,
        center: ufa_coord,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    };

    $('[map=google]').each(function() {
        var widget =  $(this);
        var widget_name = widget.attr('name');
        var map_dom = document.createElement('div');
        var map_dom_id = 'map-canvas-'+widget_name;
        map_dom.id = map_dom_id;
        map_dom.className = 'map-canvas';
        widget.after(map_dom);
        map[widget_name] = new google.maps.Map(document.getElementById(map_dom_id), mapOptions);
        markers[widget_name] = {};

        if(widget.attr('mode')=='readonly') {
            var coord_raw = widget.data();
            var coord;
            for(var key in coord_raw) {
                coord = coord_raw[key].split(';');
                markers[widget_name][key] = new google.maps.Marker({
                    map: map[widget_name],
                    icon: (key in marker_images) ? marker_images[key] : image_green,
                    shadow: pinShadow,
                    visible: true,
                    position: new google.maps.LatLng(coord[0], coord[1])
                });
            }
        } else {
            google.maps.event.addListener(map[widget_name], 'click', function (event) {
                addMarker(event.latLng, widget_name);
            });


            var clearMarkersDiv = document.createElement('div');
            var clearMarkersControl = new ClearMarkersControl(clearMarkersDiv, widget_name);
            map[widget_name].controls[google.maps.ControlPosition.TOP_RIGHT].push(clearMarkersDiv);

            var inputs = widget.data();

            var icon, icon_img;
            for (var key in inputs) {
                markers[widget_name][inputs[key]] = new google.maps.Marker({
                    map: map[widget_name],
                    icon: (key in marker_images) ? marker_images[key] : image_green,
                    shadow: pinShadow,
                    visible: false
                });
                icon = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + ( key in pinColors ? pinColors[key] : pinColors['default']);
                icon_img = ' <img height="20px" src="' + icon + '">';
                $('#' + inputs[key]).parent().children(':first-child').append(icon_img);
                $('#' + inputs[key]).attr('placeholder', 'Выберите место через карту');
                if($('#'+inputs[key]+'_lat').val() && $('#'+inputs[key]+'_lng').val()) {
                    markers[widget_name][inputs[key]].setPosition(
                        new google.maps.LatLng($('#'+inputs[key]+'_lat').val(), $('#'+inputs[key]+'_lng').val())
                    );
                    markers[widget_name][inputs[key]].setVisible(true);
                }
            }
        }
    });

}


function setPositionFromMap(latlng, position) {
    geocoder.geocode({'latLng': latlng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                $('#'+position).val(results[0].address_components[1].long_name + ', ' + results[0].address_components[0].long_name);
                $('#'+position+'_lat').val(results[0].geometry.location.A);
                $('#'+position+'_lng').val(results[0].geometry.location.F);
            } else {
                alert('No results found');
            }
        } else {
            alert('Geocoder failed due to: ' + status);
        }
    });
}


// Add a marker to the map and push to the array.
function addMarker(location, widget_name) {
    for(var key in markers[widget_name]) {
        if (markers[widget_name][key].getVisible() == false) {
            markers[widget_name][key].setVisible(true);
            markers[widget_name][key].setPosition(location);
            setPositionFromMap(location, key);
            break;
        }
    }
}


function ClearMarkersControl(controlDiv, widget_name) {

    // Set CSS for the control border
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginTop = '10px';
    controlUI.style.marginRight = '10px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Нажмите, чтобы убрать маркеры';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Очистить';
    controlUI.appendChild(controlText);


    google.maps.event.addDomListener(controlUI, 'click', function () {
        for (var key in markers[widget_name]) {
            markers[widget_name][key].setVisible(false);
            $('#'+key).val('');
            $('#'+key+'_lat').val('');
            $('#'+key+'_lng').val('');
        }
    });

}


google.maps.event.addDomListener(window, 'load', initialize);