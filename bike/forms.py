# -*- coding: utf-8 -*-
__author__ = 'bilal'

from bike.models import BikeRoute
from django.forms import ModelForm, Textarea, CharField, TextInput, HiddenInput


class BikeRoutesCreateForm(ModelForm):
    google_map_field = CharField(label='', required=False, widget=TextInput(attrs={'data-begin': 'id_location_begin',
                                                                         'data-end': 'id_location_end',
                                                                         'map': 'google',
                                                                         'hidden': 'true'}))

    class Meta:
        model = BikeRoute
        exclude = ['author', 'users_joined', 'users_joined_count', 'date_pub']
        widgets = {
            'description': Textarea(attrs={'cols': 80, 'rows': 7}),
            'location_begin': TextInput(attrs={'readonly': 'readonly'}),
            'location_end': TextInput(attrs={'readonly': 'readonly'}),
            'location_begin_lat': HiddenInput(),
            'location_begin_lng': HiddenInput(),
            'location_end_lat': HiddenInput(),
            'location_end_lng': HiddenInput(),
        }
        labels = {
            'title': 'Название',
            'description': 'Описание',
            'location_begin': 'Начало',
            'location_end': 'Конец',
            'date_start': 'Дата начала',
        }

    class Media:
        js = (
            #'http://code.jquery.com/jquery-2.1.1.min.js',
            'https://maps.googleapis.com/maps/api/js?key=AIzaSyAeYANCLPqEO1Fbxlw6DsvWlBj3jvGECFA',
            'https://maps.googleapis.com/maps/api/js?libraries=places',
            'bike/map.js',
        )
        css = {
            'all': ('bike/map.css',)
        }