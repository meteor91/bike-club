# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings


class BikeRoute(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='bike_routes')
    title = models.CharField(max_length=127)
    description = models.CharField(max_length=1023)
    date_pub = models.DateTimeField('publication_date')
    date_start = models.DateTimeField('expired date')
    location_begin = models.CharField(max_length=127)
    location_begin_lat = models.FloatField()
    location_begin_lng = models.FloatField()
    location_end = models.CharField(max_length=127)
    location_end_lat = models.FloatField()
    location_end_lng = models.FloatField()
    users_joined = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='bike_routes_joined')
    users_joined_count = models.IntegerField(default=0)


class Notification(models.Model):
    message = models.CharField(max_length=127)
    created_at = models.DateTimeField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='notifications')
    bike_route = models.ForeignKey(BikeRoute)
    notified = models.BooleanField(default=False)
    notified_at = models.DateTimeField(null=True)