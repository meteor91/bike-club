# -*- coding: utf-8 -*-
__author__ = 'bilal'

from django.conf.urls import url
from django.contrib import admin
import bike.views as views

from django.contrib.auth.decorators import login_required


admin.autodiscover()
urlpatterns = [
    url(r'^$', views.BikeRouteListView.as_view(), name='index'),
    url(r'create/$', login_required(views.BikeRouteCreateView.as_view()), name='create'),
    url(r'update/(?P<pk>[-\w]+)/$', login_required(views.BikeRouteUpdateView.as_view()), name='update'),
    url(r'detail/(?P<pk>[-\w]+)/$', views.BikeRouteDetailView.as_view(), name='detail'),
    url(r'join/(?P<br_id>[-\w]+)/$', login_required(views.JoinView.as_view()), name='join'),
    url(r'notifications', login_required(views.NotificationListView.as_view()), name='notifications')

]