# -*- coding: utf-8 -*-
from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import redirect
from django.views.generic import FormView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from user.models import User

class RegistrationForm(UserCreationForm):

    class Meta:
        model = User
        fields = ("username", "email", 'password1', "password2")

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        #user.first_name = self.cleaned_data["first_name"]
        #user.last_name = self.cleaned_data["last_name"]
        #user.username = self.cleaned_data["username"]
        #user.email = self.cleaned_data["email"]
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class RegisterFormView(FormView):
    form_class = RegistrationForm
    template_name = "user/register.html"

    def get_success_url(self):
        return reverse('login')

    def form_valid(self, form):
        form.save()
        print("login-url:")
        print(reverse("login"))
        return super(RegisterFormView, self).form_valid(form)

    def form_invalid(self, form):
        print("invalid")
        return super(RegisterFormView, self).form_invalid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "user/login.html"
    success_url = "/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        redirect_url = self.request.GET.get('next', False)
        if redirect_url:
            return redirect(redirect_url)
        return super(LoginFormView, self).form_valid(form)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")