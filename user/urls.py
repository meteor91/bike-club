# -*- coding: utf-8 -*-
__author__ = 'bilal'

from django.conf.urls import url
from django.contrib import admin
import user.views as views

admin.autodiscover()
urlpatterns = [
    url(r'register/$', views.RegisterFormView.as_view(), name='register'),
    url(r'login/$', views.LoginFormView.as_view(), name='login'),
    url(r'logout/$', views.logout_view, name='logout'),
]