# -*- coding: utf-8 -*-
from django.contrib import admin
from user.models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django import forms


class OwnUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class OwnUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


class OwnUserAdmin(UserAdmin):
    form = OwnUserChangeForm
    add_form = OwnUserCreationForm
    fieldsets = UserAdmin.fieldsets

    # Add field to personal data
    fieldsets[1][1]['fields'] = ('first_name', 'last_name', 'email', 'bike')


admin.site.register(User, OwnUserAdmin)