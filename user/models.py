# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _
# Create your models here.


class User(AbstractUser):
    bike = models.CharField(_('bike'), max_length=31)

    def notification_count(self):
        return self.notifications.count()